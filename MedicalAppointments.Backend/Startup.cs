﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MedicalAppointments.Backend.Startup))]
namespace MedicalAppointments.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
