﻿
namespace MedicalAppointments.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Appointment
    {
        [Key]
        public int AppointmentId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }
    }
}
