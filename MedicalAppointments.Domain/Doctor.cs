﻿
namespace MedicalAppointments.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Doctor
    {
        [Key]
        public int DoctorId { get; set; }

        public string IdDoctor { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Specialty { get; set; }

        public string Init { get; set; }

        public string Ends { get; set; }

        [Display(Name = "Image")]
        public string ImagePath { get; set; }
    }
}
