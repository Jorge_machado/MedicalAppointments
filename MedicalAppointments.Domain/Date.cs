﻿
namespace MedicalAppointments.Domain
{
    using System.ComponentModel.DataAnnotations;

    public class Date
    {
        [Key]
        public int DateId { get; set; }

        public string DateInit { get; set; }

        public string DateEnd { get; set; }

        public string TimeInit { get; set; }

        public string TimeEnd { get; set; }
    }
}
