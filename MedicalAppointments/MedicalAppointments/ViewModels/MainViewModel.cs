﻿
namespace MedicalAppointments.ViewModels
{
    using MedicalAppointments.Models;
    using System;
    using System.Collections.ObjectModel;

    public class MainViewModel
    {
        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }

        #region Properties
        public TokenResponse Token
        {
            get;
            set;
        } 
        #endregion

        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }

        public AppointmentsViewModel Appointments
        {
            get;
            set;
        }

        public MedicalAppointmentsViewModel MedicalAppointments
        {
            get;
            set;
        }

        public RegisterViewModel Register
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
            this.LoadMenu();
        }

        #endregion

        #region Singleton
        private static MainViewModel instance;


        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Methods
        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ConfigU",
                PageName = "MyProfilePage",
                Title = "MyProfile"
            });
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "CalendarU",
                PageName = "DatePage",
                Title = "Calendar"
            });
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "NotesU",
                PageName = "NotesPage",
                Title = "Notes"
            });
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "VideosU",
                PageName = "VideosPage",
                Title = "Videos"
            });
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ExitU",
                PageName = "LoginPage",
                Title = "Exit"
            });
        } 
        #endregion

    }
}
