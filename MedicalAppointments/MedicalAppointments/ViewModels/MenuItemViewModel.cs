﻿
namespace MedicalAppointments.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;

    public class MenuItemViewModel
    {
        #region Properties
        //Here is defined the icon that will have the menu
        public string Icon { get; set; }

        //here is defined the title that will have the menu
        public string Title { get; set; }

        //here is defined to which page we are going to reference
        public string PageName { get; set; }
        #endregion

        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }

        private async void Navigate()
        {
            if (this.PageName == "LoginPage")
            {
                Application.Current.MainPage = new LoginPage();
            }
            if (this.PageName == "DatePage")
            {
                //await Application.Current.MainPage.Navigation.PopAsync();
                //Application.Current.MainPage = new DatePage();

                //Application.Current.MainPage = new NavigationPage(
                //    new DatePage());

                App.Navigator.PushAsync(new DatePage());
                
            }
        }
        #endregion
    }
}
