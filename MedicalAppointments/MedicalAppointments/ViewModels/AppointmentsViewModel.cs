﻿
namespace MedicalAppointments.ViewModels
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Services;
    using Xamarin.Forms;

    public class AppointmentsViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<Appointment> appointments;
        #endregion

        #region Properties
        public ObservableCollection<Appointment> Appointments
        {
            get { return this.appointments; }
            set { SetValue(ref this.appointments, value); }
        }
        #endregion

        #region Constructor
        public AppointmentsViewModel()
        {
            this.apiService = new ApiService();
            this.LoadAppointments();
        }
        #endregion

        #region Methods
        private async void LoadAppointments()
        {
            var response = await this.apiService.GetList<Appointment>(
                "http://medicalappointmentsapi1.azurewebsites.net",
                "/api",
                "/Appointments");

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "response.Message",
                    "Accept");
                return;
            }

            var list = (List<Appointment>)response.Result;
            this.Appointments = new ObservableCollection<Appointment>(list);
        } 
        #endregion
    }
}
