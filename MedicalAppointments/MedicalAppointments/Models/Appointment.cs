﻿
namespace MedicalAppointments.Models
{
    using Newtonsoft.Json;

    public class Appointment
    {
        [JsonProperty(PropertyName = "appointmentId")]
        public int AppointmentId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "time")]
        public string Time { get; set; }
    }
}
