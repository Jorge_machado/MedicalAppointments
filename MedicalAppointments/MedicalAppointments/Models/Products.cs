﻿
namespace MedicalAppointments.Models
{
    using Newtonsoft.Json;

    public class Products
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("capital")]
        public string Capital { get; set; }

        [JsonProperty("subregion")]
        public string Subregion { get; set; }

        [JsonProperty("population")]
        public int Population { get; set; }

        [JsonProperty("demonym")]
        public string Demonym { get; set; }

        [JsonProperty("gini")]
        public double? Gini { get; set; }
    }

    
        
    
}
