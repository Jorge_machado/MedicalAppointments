﻿
namespace MedicalAppointments.Views
{
    using MedicalAppointments.Models;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Net.Http;
    using Xamarin.Forms;


    public partial class ProfessionalsPage : ContentPage
	{
		public ProfessionalsPage ()
		{
			InitializeComponent ();

            GetProducts();
        }
        private async void GetProducts()
        {
            HttpClient client = new HttpClient();


            var response = await client.GetStringAsync("http://restcountries.eu/rest/v2/all");

            var products = JsonConvert.DeserializeObject<List<Products>>(response);

            ProductsListView.ItemsSource = products;

        }
    }
}