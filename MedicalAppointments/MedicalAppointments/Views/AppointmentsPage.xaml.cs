﻿
namespace MedicalAppointments.Views
{
    using Syncfusion.SfSchedule.XForms;
    using System;
    using Xamarin.Forms;

	public partial class AppointmentsPage : ContentPage
	{
        
        public AppointmentsPage ()
		{
            SfSchedule schedule;
            InitializeComponent ();
            //Creating new instance for SfSchedule   
            schedule = new SfSchedule();
            MainPage = new ContentPage { Content = schedule };

            schedule.ScheduleView = ScheduleView.DayView;
            ScheduleAppointmentCollection scheduleAppointmentCollection = new ScheduleAppointmentCollection();
            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 07, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 08, 0, 0),
                Subject = "No Disponible",
                Color = Color.FromHex("#FFD80073")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 08, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 09, 0, 0),
                Subject = "Disponible",
                Color = Color.FromHex("#FFA2C139")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 09, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 10, 0, 0),
                Subject = "Disponible",
                Color = Color.FromHex("#FFA2C139")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 10, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 11, 0, 0),
                Subject = "Disponible",
                Color = Color.FromHex("#FFA2C139")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 11, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 12, 0, 0),
                Subject = "Disponible",
                Color = Color.FromHex("#FFA2C139")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 12, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 13, 0, 0),
                Subject = "Launch",
                Color = Color.FromHex("#000000")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 13, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 14, 0, 0),
                Subject = "No Disponible",
                Color = Color.FromHex("#FFD80073")
            });

            scheduleAppointmentCollection.Add(new ScheduleAppointment()
            {
                StartTime = new DateTime(2018, 5, 20, 14, 0, 0),
                EndTime = new DateTime(2018, 5, 20, 15, 0, 0),
                Subject = "Disponible",
                Color = Color.FromHex("#FFA2C139")
            });

            schedule.DataSource = scheduleAppointmentCollection;

            this.Content = schedule;
        }

        public ContentPage MainPage { get; private set; }
    }
}