﻿
namespace MedicalAppointments.Views
{
    using MedicalAppointments.Models;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Net.Http;
    using Xamarin.Forms;

    public partial class DatePage : ContentPage
	{
		public DatePage ()
		{
			InitializeComponent ();

            GetBibles();
        }
        private async void GetBibles()
        {
            HttpClient client = new HttpClient();


            var response = await client.GetStringAsync("http://restcountries.eu/rest/v2/all");

            var bibles = JsonConvert.DeserializeObject<List<Bibles>>(response);

            BiblesListView.ItemsSource = bibles;

        }
    }
}